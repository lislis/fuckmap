# F.U.C.K. map

![screenshot](screenshot.png)

## Add a F.U.C.K. space

Add an entry to `static/fucks.json`.

You can have multiple contacts, they are free-form. Just add multiple objects to the array.

There is an empty marker at `static/markers/marker.png` and the GIMP file at `static/markers/marker-all.xcf` that you can use to make your own marker, or reference the standard `static/markers/marker-fuck.png`.

You can figure out the coordinates to your city [with this service](https://www.latlong.net/). Please note that in the JSON they are `[long, lat]`.


``` json
{
  "name": "Name of the space",
  "city": "city of space",
  "coords": [11.58639, 50.92722],
  "img": "static/markers/marker-fuck.png",
  "contacts": [
      {
        "title": "Used in title attribute",
        "url": "https://url.tld",
        "value": "visible link"
      }
  ]
}
```

## Install dependencies

This is a Hugo page.

Get Hugo form the [GitHub release page](https://github.com/gohugoio/hugo/releases) or via your package manager.

## Dev server

``` bash
$ hugo serve
```

## Build site

``` bash
$ hugo
```
The build step outputs a `public/` folder which contains the entire site. This is also what happens during CI.
